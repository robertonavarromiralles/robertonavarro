import { detailEvent } from './events.service';
//import { IEvents } from './../../interfaces/i-events';
import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { IEvents } from 'interfaces/i-events';
import {map, catchError, retry} from 'rxjs/operators';

export interface EventsResponse{
  ok:boolean;
  events?:IEvents[];
  error?: string;
}
export interface EventResponse{
  ok:boolean;
  events?:IEvents;
  error?: string;
}
export interface NewEvent{
  ok: boolean;
  event: IEvents;
  error?: string;
}
export interface deleteEvent{
  ok:boolean;
  error?:string;
}
export interface detailEvent{
  ok:boolean;
  event: IEvents;
  error?:string;
}
@Injectable({
  providedIn: 'root'
})
export class EventsService {
  private eventosUrl='http://arturober.com/angular-semana4/events';
  /*getEventos()
  {
    return [{title: "Tarjeta Gráfica",
    image: "../assets/grafica.jpg",
    date: "1918-02-12",
    description: "Una tarjeta gráfica más",
    price: 58.32},{title: "Torre PC",
    image: "../assets/torre.jpg",
    date: "1996-04-25",
    description: "Es una torre de pc",
    price: 69.70},{title: "Memoria RAM",
    image: "../assets/RAM.jpg",
    date: "1956-07-01",
    description: "Es una RAM de pc",
    price: 25.70}];
  }*/
  constructor(private http: HttpClient) { }
  getEventos(): Observable<IEvents[]>{
    return this.http.get<EventsResponse>(this.eventosUrl).pipe(
      map(
        response => response.events
      )
    );
  }
  addEvents(evento: IEvents):Observable<IEvents>{
    return this.http.post<NewEvent>(this.eventosUrl, evento).pipe(catchError((resp: HttpErrorResponse)=>throwError('Error insertando un evento. Código de servidor: ${resp.status}. Mensaje: ${res.message}')),
    map(resp=>{
      if(!resp.ok){throw resp.error}
      return resp.event;
    }
    ));
  }
  deleteEvents(idEvento: number): Observable<boolean>{
    return this.http.delete<deleteEvent>(this.eventosUrl + '/' + idEvento).pipe(catchError((resp:HttpErrorResponse)=>throwError('Error insertando un evento. Código de servidor: ${resp.status}. Mensaje: ${res.message}')),
    map(resp=>{
      if(!resp.ok){throw resp.error}
      return resp.ok;
    }
    ));
  }
  getEvento(id:number): Observable<IEvents>{
    return this.http.get<detailEvent>(this.eventosUrl + '/' + id).pipe(
      map(
        response => response.event
      ),catchError((resp: HttpErrorResponse)=>
      throwError('Error obteniendo productos. Código de servidor: ${resp.status}. Mensaje: ${resp.message}'))
    );
  }
}

