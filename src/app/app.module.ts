import { MenuModule } from './menu/menu.module';
import { EventosModule } from './eventos/eventos.module';
import { EventLeavePageGuard } from './guards/event-leave-page.guard';
import { EventDetailGuard } from './guards/event-detail.guard';
import { EventsService } from './events.service';
import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import { AppComponent } from './app.component';
import { EventsShowComponent } from './events-show/events-show.component';
import { FormsModule } from '@angular/forms';
import { FilterEventsPipe } from './pipes/filter-events.pipe';
import { FiltroFechaPipe } from './pipes/filtro-fecha.pipe';
import { FiltroPrecioPipe } from './pipes/filtro-precio.pipe';
import { EventItemComponent } from './event-item/event-item.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { RouterModule, Route } from '@angular/router';
import { EventDetailComponent } from './event-detail/event-detail.component';
import { EventAddComponent } from './event-add/event-add.component';
import { APP_ROUTES } from './app.routes';


@NgModule({
  declarations: [
   /* AppComponent,
    EventsShowComponent,
    FilterEventsPipe,
    FiltroFechaPipe,
    FiltroPrecioPipe,
    EventItemComponent,
    WelcomeComponent, 
    //RouterModule.forRoot(APP_ROUTES),
    EventDetailComponent,
    EventAddComponent*/
    AppComponent,
    WelcomeComponent
  ],
  imports: [
    EventosModule, 
    BrowserModule,
    RouterModule.forRoot(APP_ROUTES),
    MenuModule
  ],
  providers: [
    EventLeavePageGuard,
    Title
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
