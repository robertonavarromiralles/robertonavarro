import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IEvents } from './../../../interfaces/i-events';
//import { EventEmitter } from 'events';

@Component({
  selector: 'roberto-app-event-item',
  templateUrl: './event-item.component.html',
  styleUrls: ['./event-item.component.scss']
})
export class EventItemComponent implements OnInit {

  constructor() { }
  @Input() evento: IEvents;
  @Output() deleteOneEvent=new EventEmitter<IEvents>();
  ngOnInit() {
  }
  deleteEvent()
  {
    this.deleteOneEvent.emit(this.evento);
  }
}