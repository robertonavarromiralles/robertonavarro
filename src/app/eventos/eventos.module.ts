import { EventsService } from './../events.service';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { FiltroPrecioPipe } from './../pipes/filtro-precio.pipe';
import { FiltroFechaPipe } from './../pipes/filtro-fecha.pipe';
import { FilterEventsPipe } from './../pipes/filter-events.pipe';
import { EventsShowComponent } from './../events-show/events-show.component';
import { EventLeavePageGuard } from './../guards/event-leave-page.guard';
import { EventAddComponent } from './../event-add/event-add.component';
import { EventDetailGuard } from './../guards/event-detail.guard';
import { EventDetailComponent } from './../event-detail/event-detail.component';
import { EventItemComponent } from './../event-item/event-item.component';
import { Route, CanActivate, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EVENTS_ROUTES } from './routes/eventos-routes';



@NgModule({
  declarations: [
    EventItemComponent,
    EventsShowComponent, 
    FilterEventsPipe, 
    EventDetailComponent, 
    FiltroFechaPipe, 
    FiltroPrecioPipe,
    EventAddComponent],
  imports: [
    CommonModule, 
    FormsModule,
    HttpClientModule, 
    RouterModule.forChild(EVENTS_ROUTES)
  ],
  providers:[
    EventsService,
    EventDetailGuard,
    EventLeavePageGuard
  ]
})
export class EventosModule { }
