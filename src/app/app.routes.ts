import { WelcomeComponent } from './welcome/welcome.component';
import { Route } from "@angular/router";

export const APP_ROUTES: Route[]=[
    {path: '', component: WelcomeComponent},
    {path:'welcome', component: WelcomeComponent},
    {path: 'events', loadChildren:'./eventos/eventos.module#EventosModule'},
    {path:'**', component: WelcomeComponent}
  ];