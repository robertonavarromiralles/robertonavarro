import { EventsService, NewEvent, deleteEvent } from './../events.service';
import { FiltroFechaPipe } from '../pipes/filtro-fecha.pipe';
import { IEvents } from './../../../interfaces/i-events';
import { Component, OnInit } from '@angular/core';
import { ValueConverter } from '@angular/compiler/src/render3/view/template';

@Component({
  selector: 'roberto-app-events-show',
  templateUrl: './events-show.component.html',
  styleUrls: ['./events-show.component.scss']
})
export class EventsShowComponent implements OnInit {

  filterSearch : string='';

  eventos: IEvents[]=[];
  
  constructor(private EventsService: EventsService) {}

  ngOnInit() {
    this.EventsService.getEventos().subscribe(
      eventos=>this.eventos=eventos,
      error=>console.error(error),
      ()=>console.log("Eventos cargados")
    );
  }
  filtraFecha()
  {
    this.eventos.sort(function(a: IEvents, b:IEvents){
    if(a.date>b.date)
      return 1;
    else if(a.date==b.date)
      return 0;
    else
      return -1;
    });
  }
  filtrarPrecio()
  {
    this.eventos.sort(function(a: IEvents, b:IEvents){
      if(a.price>b.price)
        return 1;
      else if(a.price==b.price)
        return 0;
      else
        return -1;
      });
  }
  deleteEvent(evento:IEvents)
  {
    this.EventsService.deleteEvents(evento.id).subscribe(
      event=>this.eventos.splice(evento.id, 1),
      error=>console.error(error),
      ()=>console.log("Eventos borrados")
    );
    //this.eventos=[...this.eventos];
    this.EventsService.getEventos().subscribe(
      eventos=>this.eventos=eventos,
      error=>console.error(error),
      ()=>console.log("Eventos cargados")
    );
    
  }
 
}


