import { IEvents } from 'interfaces/i-events';
import { EventsService } from './../events.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'roberto-app-event-detail',
  templateUrl: './event-detail.component.html',
  styleUrls: ['./event-detail.component.scss']
})
export class EventDetailComponent implements OnInit {

  event: IEvents
  constructor(private router: Router ,private route: ActivatedRoute, private eventService: EventsService) { }

  ngOnInit() {
    const id=+this.route.snapshot.params['id'];
    //Se supone que en el getProducts() va el id entre parentesis pero no me lo coge
    this.eventService.getEvento(id).subscribe(
      p=> this.event=p,
      error=> console.error(error)
      );
  }
  goBack(){
    this.router.navigate(['events']);
  }

}
