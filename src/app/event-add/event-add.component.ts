import { Router } from '@angular/router';

import { EventsService, NewEvent, deleteEvent } from './../events.service';
import { Component, OnInit } from '@angular/core';
import { IEvents } from 'interfaces/i-events';

@Component({
  selector: 'roberto-app-event-add',
  templateUrl: './event-add.component.html',
  styleUrls: ['./event-add.component.scss']
})
export class EventAddComponent implements OnInit {

  constructor(private EventsService: EventsService, private router: Router) { }

  eventos: IEvents[]=[];
  ngOnInit() {
  }

  newEvent: IEvents ={
    title: '',
    description: '',
    image: '',
    price:0,
    date: null
  };

  addEvent(){
    this.EventsService.addEvents(this.newEvent).subscribe(
      event=>this.eventos.push(event),
      error=>console.error(error),
      ()=>console.log("Eventos cargados")
    );
    this.newEvent={
      title: '',
      description: '',
      image: '',
      price:0,
      date: null
    };
    
    //location.reload();
    this.router.navigate(['events']);
  }

  changeImage(fileInput:HTMLInputElement){
    if(!fileInput||fileInput.files.length == 0)
      return;
    const reader: FileReader = new FileReader();

    reader.readAsDataURL(fileInput.files[0]);
    reader.addEventListener('loadend', e =>{
      this.newEvent.image=reader.result.toString();
    })
  }
}
