export interface IEvents {
    id?:number;
    title: string;
    image: string;
    date: string;
    description: string;
    price: number;
}
